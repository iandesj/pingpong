using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PingPong.Domain.Core.Models;

namespace PingPong.Infrastructure.Data.Context
{
    public class PingPongContext : DbContext
    {
        public DbSet<Club> Clubs { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<ClubPlayer> ClubPlayers { get; set; }
        public DbSet<Match> Matches { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Club>()
                .HasMany(c => c.Matches);

            modelBuilder.Entity<Club>()
                .HasOne(c => c.OwningPlayer);

            modelBuilder.Entity<Match>()
                .HasOne(m => m.HomePlayer)
                .WithMany(m => m.Matches)
                .HasForeignKey(m => m.HomePlayerId);

            modelBuilder.Entity<Match>()
                .HasOne(m => m.VisitingPlayer)
                .WithMany(m => m.Matches)
                .HasForeignKey(m => m.VisitingPlayerId);
            
            modelBuilder.Entity<Match>()
                .HasOne(m => m.Club);
            
            modelBuilder.Entity<ClubPlayer>()
                .HasKey(cp => new {cp.ClubId, cp.PlayerId});

            modelBuilder.Entity<ClubPlayer>()
                .HasOne(cp => cp.Club)
                .WithMany(cp => cp.ClubPlayers)
                .HasForeignKey(cp => cp.ClubId);

            modelBuilder.Entity<ClubPlayer>()
                .HasOne(cp => cp.Player)
                .WithMany(cp => cp.ClubPlayers)
                .HasForeignKey(cp => cp.PlayerId);
            
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // get the configuration from the app settings
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            
            // define the database to use
            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
        }
    }
}
