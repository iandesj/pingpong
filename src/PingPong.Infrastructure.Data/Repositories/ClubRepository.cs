using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using PingPong.Domain.Clubs;
using PingPong.Domain.Core.Models;
using PingPong.Infrastructure.Data.Context;

namespace PingPong.Infrastructure.Data.Repositories
{
    public class ClubRepository : Repository<Club>, IClubRepository
    {
        public ClubRepository(PingPongContext context) : base(context)
        {
        }

        public override Club FindById(Guid id)
        {
            return DbSet
                .Include(club => club.OwningPlayer)
                .Include(club => club.ClubPlayers)
                .ThenInclude(clubPlayers => clubPlayers.Player)
                .Include(club => club.Matches)
                .ThenInclude(clubMatch => new { clubMatch.HomePlayer, clubMatch.VisitingPlayer } )
                .AsNoTracking()
                .SingleOrDefault(club => club.ClubId == id);
        }
    }
}