using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PingPong.Domain.Core.Shared;
using PingPong.Infrastructure.Data.Context;

namespace PingPong.Infrastructure.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly PingPongContext _context;
        protected readonly DbSet<TEntity> DbSet;

        public Repository(PingPongContext context)
        {
            _context = context;
            DbSet = _context.Set<TEntity>();
        }
        
        public virtual TEntity FindById(Guid id)
        {
            return DbSet.Find(id);
        }

        public virtual IList<TEntity> GetAll()
        {
            return DbSet
                .AsNoTracking()
                .ToList();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}