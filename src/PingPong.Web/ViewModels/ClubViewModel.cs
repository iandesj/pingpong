using System;

namespace PingPong.Web.ViewModels
{
    public class ClubViewModel
    {
        public ClubViewModel(Guid id)
        {
            Id = id;
        }
        
        public Guid Id { get; set; }
    }
}