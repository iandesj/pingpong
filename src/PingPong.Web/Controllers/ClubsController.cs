using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PingPong.Domain.Clubs;
using PingPong.Domain.Core.Models;

namespace PingPong.Web.Controllers
{
    public class ClubsController
    {
        private readonly IClubQueryHandler _clubQueryHandler;

        public ClubsController(IClubQueryHandler clubQueryHandler)
        {
            _clubQueryHandler = clubQueryHandler;
        }

        public async Task<IActionResult> GetOne(Guid id)
        {
            var result = await _clubQueryHandler.Handle(new GetClubById(id));
            if (result == null)
                return new NotFoundResult();
            return new OkObjectResult(result);
        }

        public async Task<IActionResult> Get(int pageNumber = 1, int itemsPerPage = 100)
        {
            var result = await _clubQueryHandler.Handle(new GetAllClubsQuery(pageNumber, itemsPerPage));
            return new OkObjectResult(result);
        }
    }
}