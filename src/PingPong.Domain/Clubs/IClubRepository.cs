using PingPong.Domain.Core.Models;
using PingPong.Domain.Core.Shared;

namespace PingPong.Domain.Clubs
{
    public interface IClubRepository : IRepository<Club>
    {
    }
}