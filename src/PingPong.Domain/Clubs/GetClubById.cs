using System;
using PingPong.Domain.Core.Shared;

namespace PingPong.Domain.Clubs
{
    public class GetClubById : IQuery
    {
        public GetClubById(Guid clubId)
        {
            ClubId = clubId;
        }
        
        public Guid ClubId { get; set; }
    }
}