using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PingPong.Domain.Core.Models;

namespace PingPong.Domain.Clubs
{
    public class ClubQueryHandler : IClubQueryHandler
    {
        private readonly IClubRepository _clubRepository;

        public ClubQueryHandler(IClubRepository clubRepository)
        {
            _clubRepository = clubRepository;
        }
        
        public async Task<Club> Handle(GetClubById message)
        {
            return _clubRepository.FindById(message.ClubId);
        }

        public async Task<IList<Club>> Handle(GetAllClubsQuery message)
        {
            var allClubs = _clubRepository.GetAll();
            return allClubs
                .Skip(message.PageSkip())
                .Take(message.ItemCountPerPage)
                .ToList();
        }
    }
}