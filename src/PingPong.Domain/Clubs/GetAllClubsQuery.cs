using PingPong.Domain.Core.Shared;

namespace PingPong.Domain.Clubs
{
    public class GetAllClubsQuery : PaginatedQuery, IQuery
    {
        public GetAllClubsQuery(int pageNumber, int itemCountPerPage) : base(pageNumber, itemCountPerPage)
        {
        }
    }
}