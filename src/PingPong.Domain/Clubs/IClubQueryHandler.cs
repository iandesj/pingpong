using System.Collections.Generic;
using System.Threading.Tasks;
using PingPong.Domain.Core.Models;
using PingPong.Domain.Core.Queries;

namespace PingPong.Domain.Clubs
{
    public interface IClubQueryHandler : IQueryHandler<GetClubById, Club>,
                                         IQueryHandler<GetAllClubsQuery, IList<Club>>
    {
    }
}