using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PingPong.Domain.Clubs;
using PingPong.Domain.Core.Queries;

namespace PingPong.Domain.Core.Models
{
    public class Club : Entity
    {
        public Club(Guid clubId)
        {
            ClubId = clubId;
            ClubPlayers = new HashSet<ClubPlayer>();
            Matches = new HashSet<Match>();
        }
        
        public Guid ClubId { get; set; }
        [Required]
        public string Name { get; set; }
        public string TagLine { get; set; }
        [Required]
        public Player OwningPlayer { get; set; }
        
        public virtual ICollection<ClubPlayer> ClubPlayers { get; set; }

        public virtual ICollection<Match> Matches { get; set; }

    }
}