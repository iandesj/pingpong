using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PingPong.Domain.Core.Models
{
    public class Player : Entity
    {
        public Player(Guid playerId)
        {
            PlayerId = playerId;
            ClubPlayers = new HashSet<ClubPlayer>();
        }
        
        public Guid PlayerId { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Required]
        public string Name { get; set; }
        public string TagLine { get; set; }
        
        public virtual ICollection<ClubPlayer> ClubPlayers { get; set; }
        public virtual ICollection<Match> Matches { get; set; }
    }
}