namespace PingPong.Domain.Core.Models
{
    public class ClubPlayer : Entity
    {
        public int ClubId { get; set; }
        public Club Club { get; set; }
        
        public int PlayerId { get; set; }
        public Player Player { get; set; }
    }
}