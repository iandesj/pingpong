using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography.X509Certificates;

namespace PingPong.Domain.Core.Models
{
    public class Match : Entity
    {
        public Match(Guid matchId)
        {
            MatchId = matchId;
        }
        
        public Guid MatchId { get; set; }
        public string MatchTitle { get; set; }
        [Required]
        public int HomePlayerScore { get; set; }
        [Required]
        public int VisitingPlayerScore { get; set; }
        
        public int HomePlayerId { get; set; }
        public Player HomePlayer { get; set; }
        
        public int VisitingPlayerId { get; set; }
        public Player VisitingPlayer { get; set; }
        
        public int ClubId { get; set; }
        public Club Club { get; set; }
    }
}