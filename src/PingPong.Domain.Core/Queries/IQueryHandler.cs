using System.Threading.Tasks;
using PingPong.Domain.Core.Shared;

namespace PingPong.Domain.Core.Queries
{
    public interface IQueryHandler<in T, TReturn> : IHandler<T> where T : IQuery
    {
        Task<TReturn> Handle(T message);
    }
}