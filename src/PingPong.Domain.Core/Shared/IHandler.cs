namespace PingPong.Domain.Core.Shared
{
    public interface IHandler<in T> where T : IMessage
    {
    }
}