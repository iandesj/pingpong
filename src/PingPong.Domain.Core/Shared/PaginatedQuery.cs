using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PingPong.Domain.Core.Shared
{
    public class PaginatedQuery
    {
        public PaginatedQuery(int pageNumber, int itemCountPerPage)
        {
            PageNumber = pageNumber;
            ItemCountPerPage = itemCountPerPage;
        }
        
        [DefaultValue(1)]
        public int PageNumber { get; set; }
        [DefaultValue(100)]
        public int ItemCountPerPage { get; set; }

        public int PageSkip()
        {
            return Math.Abs(PageNumber > 1 ? (PageNumber - 1) * ItemCountPerPage : 0);
        }
    }
}