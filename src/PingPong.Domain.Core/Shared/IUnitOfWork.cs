using System;

namespace PingPong.Domain.Core.Shared
{
    public interface IUnitOfWork : IDisposable
    {
        bool Commit();
    }
}