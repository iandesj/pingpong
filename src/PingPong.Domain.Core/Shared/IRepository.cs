using System;
using System.Collections.Generic;

namespace PingPong.Domain.Core.Shared
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        TEntity FindById(Guid id);
        IList<TEntity> GetAll();
        int SaveChanges();
    }
}