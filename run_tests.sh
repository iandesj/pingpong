#!/usr/bin/env bash

dotnet test -v q tests/PingPong.Web.Tests/PingPong.Web.Tests.csproj /p:CollectCoverage=true
dotnet test -v q tests/PingPong.Domain.Tests/PingPong.Domain.Tests.csproj /p:CollectCoverage=true
