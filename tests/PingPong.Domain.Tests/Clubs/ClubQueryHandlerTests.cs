using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using PingPong.Domain.Clubs;
using PingPong.Domain.Core.Models;
using Xunit;

namespace PingPong.Domain.Tests.Clubs
{
    public class ClubQueryHandlerTests
    {
        [Fact]
        public async Task Handle_GetClubById_ReturnsClub_IfIdIsValid()
        {
            var getClubQuery = new GetClubById(Guid.NewGuid());
            var repository = new Mock<IClubRepository>();
            var handler = new ClubQueryHandler(repository.Object);

            repository.Setup(x => x.FindById(It.IsAny<Guid>())).Returns(new Club(getClubQuery.ClubId));
            var result = await handler.Handle(getClubQuery);
                        
            Assert.NotNull(result);
            Assert.Equal(result.ClubId, getClubQuery.ClubId);
        }
        
        [Fact]
        public async Task Handle_GetClubById_ReturnsNull_IfIdIsInvalid()
        {
            var getClubQuery = new GetClubById(Guid.NewGuid());
            var repository = new Mock<IClubRepository>();
            var handler = new ClubQueryHandler(repository.Object);
            
            repository.Setup(x => x.FindById(It.IsAny<Guid>())).Returns((Club)null);
            var result = await handler.Handle(getClubQuery);
                        
            Assert.Null(result);
        }

        [InlineData(1, 2)]
        [InlineData(1, 4)]
        [InlineData(2, 2)]
        [InlineData(-1, 2)]
        [Theory]
        public async Task Handle_GetAllClubsWithPagination_ReturnsClubsIfExists(int pageNumber, int itemsPerPage)
        {
            var getClubsQuery = new GetAllClubsQuery(pageNumber, itemsPerPage);
            var repository = new Mock<IClubRepository>();
            var handler = new ClubQueryHandler(repository.Object);

            repository.Setup(x => x.GetAll()).Returns(new List<Club>()
                {new Club(Guid.NewGuid()), new Club(Guid.NewGuid()),
                 new Club(Guid.NewGuid()), new Club(Guid.NewGuid())});
            var result = await handler.Handle(getClubsQuery);
            
            Assert.Equal(result.Count, itemsPerPage);
        }

        [Fact]
        public async Task Handle_GetAllClubsWithPagination_ReturnsEmptyIfNoneExist()
        {
            var getClubsQuery = new GetAllClubsQuery(1, 2);
            var repository = new Mock<IClubRepository>();
            var handler = new ClubQueryHandler(repository.Object);

            repository.Setup(x => x.GetAll()).Returns(new List<Club>());
            var result = await handler.Handle(getClubsQuery);
            
            Assert.Empty(result);
        }
    }
}