using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using PingPong.Domain.Clubs;
using PingPong.Domain.Core.Models;
using PingPong.Web.Controllers;
using Xunit;

namespace PingPong.Web.Tests.Controllers
{
    public class ClubControllerTests
    {
        [Fact]
        public async Task GetClub_ReturnsSuccess_IfClubIsValid()
        {
            var queryHandler = new Mock<IClubQueryHandler>();
            var clubController = new ClubsController(queryHandler.Object);

            var getClubById = new GetClubById(Guid.NewGuid());
            var queryResult = new Club(getClubById.ClubId);
            queryHandler.Setup(x => x.Handle(It.IsAny<GetClubById>())).Returns(Task.FromResult(queryResult));

            var actionResult = await clubController.GetOne(getClubById.ClubId);
            var getClubResult = actionResult as OkObjectResult;
            var clubResponse = getClubResult.Value as Club;

            Assert.Equal(getClubResult.StatusCode, 200);
            Assert.Equal(clubResponse.ClubId, getClubById.ClubId);
        }
        
        [Fact]
        public async Task GetClub_ReturnsNotFound_IfClubIsInvalid()
        {
            var queryHandler = new Mock<IClubQueryHandler>();
            var clubController = new ClubsController(queryHandler.Object);

            var getClubById = new GetClubById(Guid.NewGuid());
            queryHandler.Setup(x => x.Handle(It.IsAny<GetClubById>())).Returns(Task.FromResult((Club)null));

            var actionResult = await clubController.GetOne(getClubById.ClubId);
            var getClubResult = actionResult as NotFoundResult;

            Assert.Equal(getClubResult.StatusCode, 404);
        }

        [Fact]
        public async Task Get_ReturnsSuccess_WithPagination_WhenSetIsEmpty()
        {
            var getClubsQuery = new GetAllClubsQuery(1, 2);
            var queryHandler = new Mock<IClubQueryHandler>();
            var clubController = new ClubsController(queryHandler.Object);

            queryHandler.Setup(x => x.Handle(It.IsAny<GetAllClubsQuery>())).Returns(Task.FromResult((IList<Club>) new List<Club>()));

            var actionResult = await clubController.Get(getClubsQuery.PageNumber, getClubsQuery.ItemCountPerPage);
            var getResult = actionResult as OkObjectResult;
            
            Assert.Empty((List<Club>) getResult.Value);
        }

        [Fact]
        public async Task Get_ReturnsSuccess_WithPagination_WhenSetIsNotEmpty()
        {
            var getClubsQuery = new GetAllClubsQuery(1, 2);
            var queryHandler = new Mock<IClubQueryHandler>();
            var clubController = new ClubsController(queryHandler.Object);

            queryHandler.Setup(x => x.Handle(It.IsAny<GetAllClubsQuery>())).Returns(Task.FromResult(
                (IList<Club>) new List<Club> { new Club(Guid.NewGuid()) } ));

            var actionResult = await clubController.Get(getClubsQuery.PageNumber, getClubsQuery.ItemCountPerPage);
            var getResult = actionResult as OkObjectResult;
            
            Assert.NotEmpty((List<Club>) getResult.Value);
        }

        [Fact]
        public async Task Get_ReturnsSuccess_WithoutProvidedPagination_WhenSetIsEmpty()
        {
            var queryHandler = new Mock<IClubQueryHandler>();
            var clubController = new ClubsController(queryHandler.Object);

            queryHandler.Setup(x => x.Handle(It.IsAny<GetAllClubsQuery>())).Returns(Task.FromResult((IList<Club>) new List<Club>()));

            var actionResult = await clubController.Get();
            var getResult = actionResult as OkObjectResult;
            
            Assert.Empty((List<Club>) getResult.Value);
        }

        [Fact]
        public async Task Get_ReturnsSuccess_WithoutProvidedPagination_WhenSetIsNotEmpty()
        {
            var queryHandler = new Mock<IClubQueryHandler>();
            var clubController = new ClubsController(queryHandler.Object);

            queryHandler.Setup(x => x.Handle(It.IsAny<GetAllClubsQuery>())).Returns(Task.FromResult(
                (IList<Club>) new List<Club> { new Club(Guid.NewGuid()) } ));

            var actionResult = await clubController.Get();
            var getResult = actionResult as OkObjectResult;
            
            Assert.NotEmpty((List<Club>) getResult.Value);
        }
    }
}