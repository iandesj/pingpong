#! env bash

start_output=$(docker start PingPong_Development)

if echo $start_output | grep -q "PingPong_Development"; then
	echo "Ping Pong Development Database Started"
else
	docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=PingPongDevelopment" \
   		-p 1433:1433 --name PingPong_Development \
   		-d mcr.microsoft.com/mssql/server:2017-latest
fi
